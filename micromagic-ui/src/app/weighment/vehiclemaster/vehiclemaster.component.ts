import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vehiclemaster',
  templateUrl: './vehiclemaster.component.html',
  styleUrls: ['./vehiclemaster.component.css']
})
export class VehiclemasterComponent implements OnInit {
  dtOptions: any = {};
  constructor() { }

  ngOnInit() {

    this.dtOptions = {
      ajax: 'data/data.json',
      columns: [{
        title: 'ID',
        data: 'id'
      }, {
        title: 'Vehicle Number',
        data: 'vehicleNumber'
      }, {
        title: 'Driver Name',
        data: 'driverName'
      }],
      // Declare the use of the extension in the dom parameter
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'columnsToggle',
        'colvis',
        'copy',
        'print',
        'excel'
      ]
    };
  }

}
