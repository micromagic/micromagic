import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { baseUrl } from 'src/app/app.component';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient, router: Router) { }


  public getVehicleList(key): Observable<any> {
    return this.http.get<any>(baseUrl + '/api/vehicle/search?key=' + key);
  }


  public getPartyList(key): Observable<any> {
    return this.http.get<any>(baseUrl + '/api/party/search?key=' + key);
  }

  public getMaterialList(key): Observable<any> {
    return this.http.get<any>(baseUrl + '/api/material/search?key=' + key);
  }

  public getNextTicketNumber(): Observable<number> {
    return this.http.get<any>(baseUrl + '/api/lov/next/ticket/no');
  }


  public saveFirstWeight(data): Observable<number> {
    return this.http.post<any>(baseUrl + '/api/weightment/first/weight', data);
  }


}
