import { ToastrManager } from 'ng6-toastr-notifications';
import { DashboardService } from './dashboard.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {



  weightForm: FormGroup;

  weightType = 'FIRST';

  ticketnumber;

  vehicleNumber;

  party;

  vehicleNumberFormatMatchesResult = (value: any) => value.vehicleDisp || '';
  vehicleNumberFormatMatchesinput = (value: any) => value.vehicleDisp || '';

  partyNameFormatMatchesResult = (value: any) => value.partyId || '';
  partyNameFormatMatchesinput = (value: any) => value.partyId || '';


  materialNameFormatMatchesResult = (value: any) => value.materialId || '';
  materialNameFormatMatchesinput = (value: any) => value.materialId || '';

  constructor(private dashboardService: DashboardService, private toastr: ToastrManager) { }

  ngOnInit() {

    this.getNextTicketNumber();

    this.weightForm = new FormGroup({
      vehicleNumber: new FormControl(Validators.required),
      partyName: new FormControl('', Validators.required),
      materialName: new FormControl('', Validators.required),
      ticketNumber: new FormControl('', Validators.required),
      amount: new FormControl(null, Validators.required),
      tareWt: new FormControl(null, Validators.required),
      grossWt: new FormControl(null, Validators.required),
      netWt: new FormControl(null, Validators.required)
    });
  }


  onWeightFormSubmit() {


  }

  validateFormData(formData) {

    formData.type = this.weightType;

    console.log(formData);

    if (formData.vehicleNumber === null || formData.vehicleNumber === undefined || formData.vehicleNumber === '') {

      this.toastr.errorToastr('Invalid Vehicle Number..!', 'Oops!');

      return null;
    }

    if (formData.vehicleNumber.vehicleNo === undefined || formData.vehicleNumber.vehicleNo === null ||
      formData.vehicleNumber.vehicleNo === '') {

      const vehicleNumber = { vehicleNo: formData.vehicleNumber };

      formData.vehicleNumber = vehicleNumber;

    }

    if (formData.partyName === null || formData.partyName === undefined || formData.partyName === '') {

      this.toastr.errorToastr('Invalid Party Name..!', 'Oops!');

      return null;
    }

    if (formData.partyName.partyId === undefined || formData.partyName.partyId === null ||
      formData.partyName.partyId === '') {

      const partyName = { partyId: formData.partyName };

      formData.partyName = partyName;

    }


    if (formData.materialName === null || formData.materialName === undefined || formData.materialName === '') {

      this.toastr.errorToastr('Invalid Material Name..!', 'Oops!');

      return null;
    }

    if (formData.materialName.materialId === undefined || formData.materialName.materialId === null ||
      formData.materialName.materialId === '') {

      const materialName = { materialId: formData.materialName };

      formData.materialName = materialName;

    }


    if (formData.amount === null || formData.amount === undefined || formData.amount === '' || isNaN(formData.amount)) {

      this.toastr.errorToastr('Invalid Amount..!', 'Oops!');

      return null;
    }


    if (formData.tareWt === null || formData.tareWt === undefined || formData.netWt === null || formData.netWt === undefined ||
      isNaN(formData.tareWt) || isNaN(formData.netWt)) {
      this.toastr.errorToastr('Invalid Weight..!', 'Oops!');
      return null;

    }

    if (formData.type === 'FIRST') {

      // tareWt
      // gorssWt
      if ((formData.tareWt !== formData.netWt) || Number(formData.tareWt < 1 || formData.netWt < 1)) {

        this.toastr.errorToastr('Invalid Weight..!', 'Oops!');
        return null;

      }


    }


    return formData;
  }


  submitClicked(type) {



    switch (type) {

      case 'SAVE':

        const finalData = this.validateFormData(this.weightForm.value);

        if (finalData != null) {


          this.dashboardService.saveFirstWeight(finalData)
            .subscribe(res => {

              this.toastr.successToastr('First Weight Added Successfully.!', 'Done!');

              this.weightForm.reset();
            },
              err => {
                this.toastr.errorToastr('Error..Please Try Again.!', 'Oops!');
              }
            );

        }


        break;

      case 'PRINTSAVE':
        console.log('CMD==>PRINT');
        console.log(this.weightType);
        console.log(this.weightForm.value);

        break;

      case 'CANCEL':
        console.log('CANCEL');

        break;
    }
  }

  requestSecandWeight(data) {

    console.log(data.ticketnumber);

  }

  weightTypeChanged(type) {
    this.weightType = type;
    if (this.weightType === 'FIRST') {
      console.log(type);
      this.getNextTicketNumber();
    }
  }


  vehicleNumberList = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((searchText) => this.dashboardService.getVehicleList(searchText))
    )




  partyNameList = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((searchText) => this.dashboardService.getPartyList(searchText))
    )


  materialNameList = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((searchText) => this.dashboardService.getMaterialList(searchText))
    )


  getNextTicketNumber() {
    this.dashboardService.getNextTicketNumber().subscribe(
      res => {
        this.ticketnumber = res;
      },
      () => {
      }
    );
  }



  selectVehicleNumber(event) {
    this.vehicleNumber = event.item.vehicleDisp;
    this.weightForm.controls.vehicleNumber.setValue(this.vehicleNumber);
  }

  selectParty(event) {

    this.party = event.item.partyId;
    this.weightForm.controls.partyName.setValue(this.party);

  }
}
