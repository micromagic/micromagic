import { CustomermasterComponent } from './weighment/customermaster/customermaster.component';
import { SuppliermasterComponent } from './weighment/suppliermaster/suppliermaster.component';
import { VehiclemasterComponent } from './weighment/vehiclemaster/vehiclemaster.component';
import { AuthComponent } from './auth/auth.component';
import { NgModule } from '@angular/core';
import { PageNotfoundComponent } from './page-notfound/page-notfound.component';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './weighment/dashboard/dashboard.component';



const routes: Routes = [
  {
    path: '', redirectTo: 'login', pathMatch: 'full'
  }, {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'auth',
    component: AuthComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'master/customer',
    component: CustomermasterComponent
  },
  {
    path: 'master/supplier',
    component: SuppliermasterComponent
  },
  {
    path: 'master/vehicle',
    component: VehiclemasterComponent
  },
  {
    path: '**',
    component: PageNotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
