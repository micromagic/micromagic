import { AppRoutingModule } from './app-routing.modules';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { httpInterceptorProviders } from './interceptor';
import { DataTablesModule } from 'angular-datatables';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PageNotfoundComponent } from './page-notfound/page-notfound.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { DashboardComponent } from './weighment/dashboard/dashboard.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HeaderComponent } from './frame/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ng6-toastr-notifications';
import {  HttpClientModule } from '@angular/common/http';
import { AuthComponent } from './auth/auth.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { VehiclemasterComponent } from './weighment/vehiclemaster/vehiclemaster.component';
import { SuppliermasterComponent } from './weighment/suppliermaster/suppliermaster.component';
import { CustomermasterComponent } from './weighment/customermaster/customermaster.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotfoundComponent,
    DashboardComponent,
    HeaderComponent,
    AuthComponent,
    VehiclemasterComponent,
    SuppliermasterComponent,
    CustomermasterComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule, DataTablesModule, AppRoutingModule, AngularFontAwesomeModule, BrowserAnimationsModule, HttpClientModule,
    ReactiveFormsModule, NgbModule, ToastrModule.forRoot()
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent],
  entryComponents: []

})
export class AppModule { }
