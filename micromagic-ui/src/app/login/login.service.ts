import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from '../app.component';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient,  public router: Router) { }

  // login module
  public login(postData): Observable<any> {
    return this.http.post<any>(baseUrl + '/openapi/login', postData);
  }


}
