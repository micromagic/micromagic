import { LoginService } from './login.service';
import { RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private router: Router, private loginService: LoginService, private toastr: ToastrManager) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      userId: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });

  }


  onLoginFormSubmit(loginFormData) {
    console.log(loginFormData);

    localStorage.clear();
    this.loginService.login(loginFormData)
      .subscribe(
        res => {
          localStorage.setItem('apiKey', res.key);
          this.router.navigateByUrl('/auth');
        },
        err => {
          this.toastr.errorToastr('Invalid UserId or Password..!', 'Oops!');
        }
      );
  }
}
