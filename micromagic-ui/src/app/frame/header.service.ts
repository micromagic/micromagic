import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from '../app.component';
@Injectable({
  providedIn: 'root'
})
export class HeaderService {
constructor(private http: HttpClient,  public router: Router) { }

public getLangList(): Observable<any> {
  return this.http.get<any>(baseUrl + '/api/lov/lang/list');
}

public getRoleList(): Observable<any> {
  return this.http.get<any>(baseUrl + '/api/lov/role/list');
}
}
