import { HeaderService } from './../header.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  menuClicked = false;
  isLangCollapsed = true;
  langList;
  roleList;
  isRoleCollapsed = true;
  isReportCollapsed = true;
  isMastersCollapsed = true;
  isSettingCollapsed = true;
  constructor(private headeService: HeaderService) { }

  ngOnInit() {

    this.headeService.getLangList()
      .subscribe(
        res => {
          this.langList = res;
          console.log(res);

        },
        err => {

        }
      );



    this.headeService.getRoleList()
      .subscribe(
        res => {
          this.roleList = res;
          console.log(res);

        },
        err => {

        }
      );
  }


  userMenuClicked() {

    if (this.menuClicked) {
      this.menuClicked = false;
    } else {
      this.menuClicked = true;
    }

  }


}
