import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

apiKey ;

  constructor(private router: Router, ) { }

  ngOnInit() {

    this.apiKey = null;

    this.apiKey = localStorage.getItem('apiKey');

    if (this.apiKey == null || this.apiKey == undefined || this.apiKey == '') {

      this.router.navigateByUrl('/login');

    } else {
      this.router.navigateByUrl('/dashboard');

    }



  }

}
