import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';

export class AddHeaderInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Clone the request to add the new header
        //  let apikey = localStorage.getItem('X-Auth-Token');
        // apikey = '$2a$10$ZfGitu23WbHcN.cZU5iGnehcwiIZqRtqzdyangm6szbE9102a04MK';
        let apikey = localStorage.getItem('apiKey');

        if (apikey == undefined) {
            apikey = '';
        }

        const clonedRequest = req.clone({ headers: req.headers.set('X-Auth-Token', apikey) });
       // const clonedRequest = req.clone({ headers: req.headers.set('apikey', apikey) });

        console.log(apikey);

        return next.handle(clonedRequest);
    }
}