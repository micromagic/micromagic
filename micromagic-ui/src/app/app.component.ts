import { Component } from '@angular/core';

import fontawesome from '@fortawesome/fontawesome';
import faR from '@fortawesome/fontawesome-free-regular/';
import faS from '@fortawesome/fontawesome-free-solid/';

export const baseUrl = 'http://localhost:8090';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'micromagic-ui';

  AppComponent() {
    fontawesome.library.add(faR);
    fontawesome.library.add(faS);

  }

}
