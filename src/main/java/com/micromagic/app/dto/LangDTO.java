package com.micromagic.app.dto;

import com.micromagic.app.entity.CVM_LANG;

public class LangDTO {
	
	String langId;
	String langName;
	
	
	public LangDTO(CVM_LANG lang) {
		this. langId = lang.getCVML_ID();
		this. langName =lang.getCVML_NAME();
	}
	public String getLangId() {
		return langId;
	}
	public void setLangId(String langId) {
		this.langId = langId;
	}
	public String getLangName() {
		return langName;
	}
	public void setLangName(String langName) {
		this.langName = langName;
	}
	

}
