package com.micromagic.app.dto;


import com.micromagic.app.entity.CVT_API_KEY;

public class ApiKeyDTO {
	String key;
	String userId;
	String roleId;
	String favLangId;
	
	
	public ApiKeyDTO(CVT_API_KEY api_key) {
		
		this.key = api_key.getCVTAK_KEY(); 
		this.roleId =api_key.getCVTAK_CVM_ROLE().getCVMR_ID();
		this.favLangId = api_key.getCVTAK_CVM_USER().getCVMU_FAV_LANG();

	}
	public ApiKeyDTO() {
		// TODO Auto-generated constructor stub
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	
	

}
