package com.micromagic.app.dto;

public class WeighMentDTO {

	Double grossWt;
	Double tareWt;
	Double netWt;

	VehicleDTO vehicleNumber;

	PartyDTO partyName;
	
	MaterialDTO materialName;
	
	Long amount;
	
	
	


	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public MaterialDTO getMaterialName() {
		return materialName;
	}

	public void setMaterialName(MaterialDTO materialName) {
		this.materialName = materialName;
	}

	public PartyDTO getPartyName() {
		return partyName;
	}

	public void setPartyName(PartyDTO partyName) {
		this.partyName = partyName;
	}

	public VehicleDTO getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(VehicleDTO vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public Double getGrossWt() {
		return grossWt;
	}

	public void setGrossWt(Double grossWt) {
		this.grossWt = grossWt;
	}

	public Double getTareWt() {
		return tareWt;
	}

	public void setTareWt(Double tareWt) {
		this.tareWt = tareWt;
	}

	public Double getNetWt() {
		return netWt;
	}

	public void setNetWt(Double netWt) {
		this.netWt = netWt;
	}

}
