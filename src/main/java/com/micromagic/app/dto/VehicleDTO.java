package com.micromagic.app.dto;

import com.micromagic.app.entity.CVM_VEHICLE;

public class VehicleDTO {

	String vehicleNo;
	String vehicleDisp;
	String vehicleType;
	
	
	public VehicleDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public VehicleDTO(CVM_VEHICLE vehicle) {
		
		this.vehicleNo = vehicle.getCVMV_ID();

	}
	
	
	
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleDisp() {
		return vehicleDisp;
	}
	public void setVehicleDisp(String vehicleDisp) {
		this.vehicleDisp = vehicleDisp;
	}
	
	
	
}
