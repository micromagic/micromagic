package com.micromagic.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "CVM_ROLE")
public class CVM_ROLE  implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	@Column(name = "CVMR_ID", unique = true)
	private String CVMR_ID;

	@Column(name = "CVMR_NAME")
	private String CVMR_NAME;
	
	public String getCVMR_ID() {
		return CVMR_ID;
	}

	public void setCVMR_ID(String cVMR_ID) {
		CVMR_ID = cVMR_ID;
	}

	public String getCVMR_NAME() {
		return CVMR_NAME;
	}

	public void setCVMR_NAME(String cVMR_NAME) {
		CVMR_NAME = cVMR_NAME;
	}
	
	
	
	
}
