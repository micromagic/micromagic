package com.micromagic.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "CVM_WEIGH_BRIDGE")
public class CVM_WEIGH_BRIDGE implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	@Column(name = "CVMWB_ID", unique = true)
	private String CVMWB_ID;

	@Column(name = "CVMWB_NAME")
	private String CVMWB_NAME;

	public String getCVMWB_ID() {
		return CVMWB_ID;
	}

	public void setCVMWB_ID(String cVMWB_ID) {
		CVMWB_ID = cVMWB_ID;
	}

	public String getCVMWB_NAME() {
		return CVMWB_NAME;
	}

	public void setCVMWB_NAME(String cVMWB_NAME) {
		CVMWB_NAME = cVMWB_NAME;
	}

	
}
