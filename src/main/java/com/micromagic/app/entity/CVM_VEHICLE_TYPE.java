package com.micromagic.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;


@Entity
@DynamicUpdate
@Table(name = "CVM_VEHICLE_TYPE")
public class CVM_VEHICLE_TYPE implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2064145288991039736L;

	@Id
	@Column(name = "CVMVT_ID", unique = true)
	private String CVMVT_ID;

	@Column(name = "CVMVT_DISC")
	private String CVMVT_DISC;

	public String getCVMVT_ID() {
		return CVMVT_ID;
	}

	public void setCVMVT_ID(String cVMVT_ID) {
		CVMVT_ID = cVMVT_ID;
	}

	public String getCVMVT_DISC() {
		return CVMVT_DISC;
	}

	public void setCVMVT_DISC(String cVMVT_DISC) {
		CVMVT_DISC = cVMVT_DISC;
	}
	
	
	
	
}
