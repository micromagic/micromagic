package com.micromagic.app.entity;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;


@Entity
@DynamicUpdate
@Table(name = "CVM_PARTY")
public class CVM_PARTY  implements Serializable{

	
	private static final long serialVersionUID = 4803807517256783310L;

	@Id
	@Column(name = "CVMP_ID", unique = true)
	private String CVMP_ID;
	
	@Column(name = "CVMP_NAME")
	private String CVMP_NAME;

	@Column(name = "CVMP_PHONE_01")
	private String CVMP_PHONE_01;

	@Column(name = "CVMP_PHONE_02")
	private String CVMP_PHONE_02;

	@Column(name = "CVMP_EMAIL")
	private String CVMP_EMAIL;

	@Column(name = "CVMP_CR_UID")
	private String CVMP_CR_UID;

	@Column(name = "CVMP_CR_DT")
	private OffsetDateTime CVMP_CR_DT;

	
	
	
	
	public String getCVMP_NAME() {
		return CVMP_NAME;
	}

	public void setCVMP_NAME(String cVMP_NAME) {
		CVMP_NAME = cVMP_NAME;
	}

	public String getCVMP_ID() {
		return CVMP_ID;
	}

	public void setCVMP_ID(String cVMP_ID) {
		CVMP_ID = cVMP_ID;
	}



	public String getCVMP_PHONE_01() {
		return CVMP_PHONE_01;
	}

	public void setCVMP_PHONE_01(String cVMP_PHONE_01) {
		CVMP_PHONE_01 = cVMP_PHONE_01;
	}

	public String getCVMP_PHONE_02() {
		return CVMP_PHONE_02;
	}

	public void setCVMP_PHONE_02(String cVMP_PHONE_02) {
		CVMP_PHONE_02 = cVMP_PHONE_02;
	}

	public String getCVMP_EMAIL() {
		return CVMP_EMAIL;
	}

	public void setCVMP_EMAIL(String cVMP_EMAIL) {
		CVMP_EMAIL = cVMP_EMAIL;
	}

	public String getCVMP_CR_UID() {
		return CVMP_CR_UID;
	}

	public void setCVMP_CR_UID(String cVMP_CR_UID) {
		CVMP_CR_UID = cVMP_CR_UID;
	}

	public OffsetDateTime getCVMP_CR_DT() {
		return CVMP_CR_DT;
	}

	public void setCVMP_CR_DT(OffsetDateTime cVMP_CR_DT) {
		CVMP_CR_DT = cVMP_CR_DT;
	}

	
	
	
	
}
