package com.micromagic.app.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "CVM_WEIGHBRIDGE_ROLE")
public class CVM_WEIGHBRIDGE_ROLE implements Serializable{


	private static final long serialVersionUID = 1493253115235416048L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CVMWR_ROLE_AG_ID", unique = true)
	private Integer CVMWR_ROLE_AG_ID;

	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CVMWR_CVM_WEIGH_BRIDGE")
	private CVM_WEIGH_BRIDGE CVMWR_CVM_WEIGH_BRIDGE;

	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CVMWR_CVM_ROLE")
	private CVM_ROLE CVMWR_CVM_ROLE;


	public Integer getCVMWR_ROLE_AG_ID() {
		return CVMWR_ROLE_AG_ID;
	}


	public void setCVMWR_ROLE_AG_ID(Integer cVMWR_ROLE_AG_ID) {
		CVMWR_ROLE_AG_ID = cVMWR_ROLE_AG_ID;
	}


	public CVM_WEIGH_BRIDGE getCVMWR_CVM_WEIGH_BRIDGE() {
		return CVMWR_CVM_WEIGH_BRIDGE;
	}


	public void setCVMWR_CVM_WEIGH_BRIDGE(CVM_WEIGH_BRIDGE cVMWR_CVM_WEIGH_BRIDGE) {
		CVMWR_CVM_WEIGH_BRIDGE = cVMWR_CVM_WEIGH_BRIDGE;
	}


	public CVM_ROLE getCVMWR_CVM_ROLE() {
		return CVMWR_CVM_ROLE;
	}


	public void setCVMWR_CVM_ROLE(CVM_ROLE cVMWR_CVM_ROLE) {
		CVMWR_CVM_ROLE = cVMWR_CVM_ROLE;
	}
	
	
	
	
	
}
