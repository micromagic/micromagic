package com.micromagic.app.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "CVM_USER_ROLE")
public class CVM_USER_ROLE implements Serializable{

	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CVMUR_AG_ID", unique = true)
	private Integer CVMUR_AG_ID;

	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CVMUR_CVM_USER")
	private CVM_USER CVMUR_CVM_USER;

	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CVMUR_CVM_ROLE")
	private CVM_ROLE CVMUR_CVM_ROLE;


	@Column(name = "CVMUR_DEFAULT_YN")
	private String CVMUR_DEFAULT_YN;
	
	public Integer getCVMUR_AG_ID() {
		return CVMUR_AG_ID;
	}


	public void setCVMUR_AG_ID(Integer cVMUR_AG_ID) {
		CVMUR_AG_ID = cVMUR_AG_ID;
	}


	public CVM_USER getCVMUR_CVM_USER() {
		return CVMUR_CVM_USER;
	}


	public void setCVMUR_CVM_USER(CVM_USER cVMUR_CVM_USER) {
		CVMUR_CVM_USER = cVMUR_CVM_USER;
	}


	public CVM_ROLE getCVMUR_CVM_ROLE() {
		return CVMUR_CVM_ROLE;
	}


	public void setCVMUR_CVM_ROLE(CVM_ROLE cVMUR_CVM_ROLE) {
		CVMUR_CVM_ROLE = cVMUR_CVM_ROLE;
	}


	public String getCVMUR_DEFAULT_YN() {
		return CVMUR_DEFAULT_YN;
	}


	public void setCVMUR_DEFAULT_YN(String cVMUR_DEFAULT_YN) {
		CVMUR_DEFAULT_YN = cVMUR_DEFAULT_YN;
	}

	
}
