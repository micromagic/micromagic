package com.micromagic.app.entity;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CVT_API_KEY")
public class CVT_API_KEY implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	@Column(name = "CVTAK_KEY", unique = true)
	private String CVTAK_KEY;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CVTAK_CVM_USER", foreignKey = @ForeignKey(name = "FK_CVTAK_CVM_USER"))
	private CVM_USER CVTAK_CVM_USER;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CVTAK_CVM_ROLE", foreignKey = @ForeignKey(name = "FK_CVTAK_CVM_ROLE"))
	private CVM_ROLE CVTAK_CVM_ROLE;

	@Column(name = "CVTAK_CR_DT")
	private OffsetDateTime CVTAK_CR_DT;

	public String getCVTAK_KEY() {
		return CVTAK_KEY;
	}

	public void setCVTAK_KEY(String cVTAK_KEY) {
		CVTAK_KEY = cVTAK_KEY;
	}

	public CVM_USER getCVTAK_CVM_USER() {
		return CVTAK_CVM_USER;
	}

	public void setCVTAK_CVM_USER(CVM_USER cVTAK_CVM_USER) {
		CVTAK_CVM_USER = cVTAK_CVM_USER;
	}

	public CVM_ROLE getCVTAK_CVM_ROLE() {
		return CVTAK_CVM_ROLE;
	}

	public void setCVTAK_CVM_ROLE(CVM_ROLE cVTAK_CVM_ROLE) {
		CVTAK_CVM_ROLE = cVTAK_CVM_ROLE;
	}

	public OffsetDateTime getCVTAK_CR_DT() {
		return CVTAK_CR_DT;
	}

	public void setCVTAK_CR_DT(OffsetDateTime cVTAK_CR_DT) {
		CVTAK_CR_DT = cVTAK_CR_DT;
	}

	
	
	
	
}
