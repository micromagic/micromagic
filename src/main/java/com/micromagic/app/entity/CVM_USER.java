package com.micromagic.app.entity;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "CVM_USER")
public class CVM_USER implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	@Column(name = "CVMU_ID", unique = true)
	private String CVMU_ID;

	@Column(name = "CVMU_NAME")
	private String CVMU_NAME;
	
	@Column(name = "CVMU_FAV_LANG")
	private String CVMU_FAV_LANG;
	
	@Column(name = "CVMU_PASSWORD")
	private String CVMU_PASSWORD;

	@Column(name = "CVMU_CR_UID")
	private String CVMU_CR_UID;

	@Column(name = "CVMU_CR_DT")
	private OffsetDateTime CVMU_CR_DT;

	@Column(name = "CVMU_DELETED_YN")
	private String CVMU_DELETED_YN;

	@Column(name = "CVMU_DELETED_UID")
	private String CVMU_DELETED_UID;

	@Column(name = "CVMU_DELETED_DT")
	private OffsetDateTime CVMU_DELETED_DT;

	
	
	
	
	public String getCVMU_FAV_LANG() {
		return CVMU_FAV_LANG;
	}

	public void setCVMU_FAV_LANG(String cVMU_FAV_LANG) {
		CVMU_FAV_LANG = cVMU_FAV_LANG;
	}

	public String getCVMU_ID() {
		return CVMU_ID;
	}

	public String getCVMU_NAME() {
		return CVMU_NAME;
	}

	public void setCVMU_NAME(String cVMU_NAME) {
		CVMU_NAME = cVMU_NAME;
	}

	public String getCVMU_CR_UID() {
		return CVMU_CR_UID;
	}

	public void setCVMU_CR_UID(String cVMU_CR_UID) {
		CVMU_CR_UID = cVMU_CR_UID;
	}

	public OffsetDateTime getCVMU_CR_DT() {
		return CVMU_CR_DT;
	}

	public void setCVMU_CR_DT(OffsetDateTime cVMU_CR_DT) {
		CVMU_CR_DT = cVMU_CR_DT;
	}

	public String getCVMU_DELETED_YN() {
		return CVMU_DELETED_YN;
	}

	public void setCVMU_DELETED_YN(String cVMU_DELETED_YN) {
		CVMU_DELETED_YN = cVMU_DELETED_YN;
	}

	public String getCVMU_DELETED_UID() {
		return CVMU_DELETED_UID;
	}

	public void setCVMU_DELETED_UID(String cVMU_DELETED_UID) {
		CVMU_DELETED_UID = cVMU_DELETED_UID;
	}

	public OffsetDateTime getCVMU_DELETED_DT() {
		return CVMU_DELETED_DT;
	}

	public void setCVMU_DELETED_DT(OffsetDateTime cVMU_DELETED_DT) {
		CVMU_DELETED_DT = cVMU_DELETED_DT;
	}

	public void setCVMU_ID(String cVMU_ID) {
		CVMU_ID = cVMU_ID;
	}

	public String getCVMU_PASSWORD() {
		return CVMU_PASSWORD;
	}

	public void setCVMU_PASSWORD(String cVMU_PASSWORD) {
		CVMU_PASSWORD = cVMU_PASSWORD;
	}


	
}
