package com.micromagic.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "CVM_LANG")
public class CVM_LANG implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	@Column(name = "CVML_ID", unique = true)
	private String CVML_ID;

	@Column(name = "CVML_NAME")
	private String CVML_NAME;

	public String getCVML_ID() {
		return CVML_ID;
	}

	public void setCVML_ID(String cVML_ID) {
		CVML_ID = cVML_ID;
	}

	public String getCVML_NAME() {
		return CVML_NAME;
	}

	public void setCVML_NAME(String cVML_NAME) {
		CVML_NAME = cVML_NAME;
	}

}
