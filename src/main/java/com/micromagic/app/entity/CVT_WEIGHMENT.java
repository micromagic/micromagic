package com.micromagic.app.entity;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "CVT_WEIGHMENT")
public class CVT_WEIGHMENT implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CVTW_AG_ID", unique = true)
	private Integer CVTW_AG_ID;

	@Column(name = "CVTW_TYPE")
	private String CVTW_TYPE;

	@Column(name = "CVTW_PARTY")
	private String CVTW_PARTY;

	@Column(name = "CVTW_SUPPLIER")
	private String CVTW_SUPPLIER;

	@Column(name = "CVTW_VEHICLE_NUMBER")
	private String CVTW_VEHICLE_NUMBER;

	@Column(name = "CVTW_ITEM_NAME")
	private String CVTW_ITEM_NAME;

	@Column(name = "CVTW_UDF_001")
	private String CVTW_UDF_001;

	@Column(name = "CVTW_UDF_002")
	private String CVTW_UDF_002;

	@Column(name = "CVTW_UDF_003")
	private String CVTW_UDF_003;

	@Column(name = "CVTW_UDF_004")
	private String CVTW_UDF_004;

	@Column(name = "CVTW_UDF_005")
	private String CVTW_UDF_005;

	@Column(name = "CVTW_FEES")
	private Long CVTW_FEES;

	@Column(name = "CVTW_PAYMENT_MODE") //Cash or Check
	private String CVTW_PAYMENT_MODE;

	@Column(name = "CVTW_GROSS_DT")
	private OffsetDateTime CVTW_GROSS_DT;

	@Column(name = "CVTW_TARE_DT")
	private OffsetDateTime CVTW_TARE_DT;

	@Column(name = "CVTW_GROSS")
	private Double CVTW_GROSS;

	@Column(name = "CVTW_TARE")
	private Double CVTW_TARE;

	@Column(name = "CVTW_NET")
	private Double CVTW_NET;

	@Column(name = "CVTW_COMMENT")
	private String CVTW_COMMENT;
	
	public Integer getCVTW_AG_ID() {
		return CVTW_AG_ID;
	}

	public void setCVTW_AG_ID(Integer cVTW_AG_ID) {
		CVTW_AG_ID = cVTW_AG_ID;
	}

	public String getCVTW_TYPE() {
		return CVTW_TYPE;
	}

	public void setCVTW_TYPE(String cVTW_TYPE) {
		CVTW_TYPE = cVTW_TYPE;
	}

	public String getCVTW_PARTY() {
		return CVTW_PARTY;
	}

	public void setCVTW_PARTY(String cVTW_PARTY) {
		CVTW_PARTY = cVTW_PARTY;
	}

	public String getCVTW_SUPPLIER() {
		return CVTW_SUPPLIER;
	}

	public void setCVTW_SUPPLIER(String cVTW_SUPPLIER) {
		CVTW_SUPPLIER = cVTW_SUPPLIER;
	}

	public String getCVTW_VEHICLE_NUMBER() {
		return CVTW_VEHICLE_NUMBER;
	}

	public void setCVTW_VEHICLE_NUMBER(String cVTW_VEHICLE_NUMBER) {
		CVTW_VEHICLE_NUMBER = cVTW_VEHICLE_NUMBER;
	}

	public String getCVTW_ITEM_NAME() {
		return CVTW_ITEM_NAME;
	}

	public void setCVTW_ITEM_NAME(String cVTW_ITEM_NAME) {
		CVTW_ITEM_NAME = cVTW_ITEM_NAME;
	}

	public String getCVTW_UDF_001() {
		return CVTW_UDF_001;
	}

	public void setCVTW_UDF_001(String cVTW_UDF_001) {
		CVTW_UDF_001 = cVTW_UDF_001;
	}

	public String getCVTW_UDF_002() {
		return CVTW_UDF_002;
	}

	public void setCVTW_UDF_002(String cVTW_UDF_002) {
		CVTW_UDF_002 = cVTW_UDF_002;
	}

	public String getCVTW_UDF_003() {
		return CVTW_UDF_003;
	}

	public void setCVTW_UDF_003(String cVTW_UDF_003) {
		CVTW_UDF_003 = cVTW_UDF_003;
	}

	public String getCVTW_UDF_004() {
		return CVTW_UDF_004;
	}

	public void setCVTW_UDF_004(String cVTW_UDF_004) {
		CVTW_UDF_004 = cVTW_UDF_004;
	}

	public String getCVTW_UDF_005() {
		return CVTW_UDF_005;
	}

	public void setCVTW_UDF_005(String cVTW_UDF_005) {
		CVTW_UDF_005 = cVTW_UDF_005;
	}

	public String getCVTW_PAYMENT_MODE() {
		return CVTW_PAYMENT_MODE;
	}

	public void setCVTW_PAYMENT_MODE(String cVTW_PAYMENT_MODE) {
		CVTW_PAYMENT_MODE = cVTW_PAYMENT_MODE;
	}

	public OffsetDateTime getCVTW_GROSS_DT() {
		return CVTW_GROSS_DT;
	}

	public void setCVTW_GROSS_DT(OffsetDateTime cVTW_GROSS_DT) {
		CVTW_GROSS_DT = cVTW_GROSS_DT;
	}

	public OffsetDateTime getCVTW_TARE_DT() {
		return CVTW_TARE_DT;
	}

	public void setCVTW_TARE_DT(OffsetDateTime cVTW_TARE_DT) {
		CVTW_TARE_DT = cVTW_TARE_DT;
	}

	public Long getCVTW_FEES() {
		return CVTW_FEES;
	}

	public void setCVTW_FEES(Long cVTW_FEES) {
		CVTW_FEES = cVTW_FEES;
	}

	public Double getCVTW_GROSS() {
		return CVTW_GROSS;
	}

	public void setCVTW_GROSS(Double cVTW_GROSS) {
		CVTW_GROSS = cVTW_GROSS;
	}

	public Double getCVTW_TARE() {
		return CVTW_TARE;
	}

	public void setCVTW_TARE(Double cVTW_TARE) {
		CVTW_TARE = cVTW_TARE;
	}

	public Double getCVTW_NET() {
		return CVTW_NET;
	}

	public void setCVTW_NET(Double cVTW_NET) {
		CVTW_NET = cVTW_NET;
	}

	public String getCVTW_COMMENT() {
		return CVTW_COMMENT;
	}

	public void setCVTW_COMMENT(String cVTW_COMMENT) {
		CVTW_COMMENT = cVTW_COMMENT;
	}
}
