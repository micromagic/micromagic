package com.micromagic.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "CVM_MATERIAL")
public class CVM_MATERIAL implements Serializable {

	private static final long serialVersionUID = 2631027029580969002L;

	@Id
	@Column(name = "CVMM_ID", unique = true)
	private String CVMM_ID;

	@Column(name = "CVMM_NAME")
	private String CVMM_NAME;


	public String getCVMM_ID() {
		return CVMM_ID;
	}

	public void setCVMM_ID(String cVMM_ID) {
		CVMM_ID = cVMM_ID;
	}

	public String getCVMM_NAME() {
		return CVMM_NAME;
	}

	public void setCVMM_NAME(String cVMM_NAME) {
		CVMM_NAME = cVMM_NAME;
	}



}
