package com.micromagic.app.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "CVM_ROLE_MENU")
public class CVM_ROLE_MENU implements Serializable{

	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CVMRM_AG_ID", unique = true)
	private Integer CVMRM_AG_ID;

	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CVMRM_CVM_MENU")
	private CVM_MENU CVMRM_CVM_MENU;

	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CVMRM_CVM_ROLE")
	private CVM_ROLE CVMRM_CVM_ROLE;


	public Integer getCVMRM_AG_ID() {
		return CVMRM_AG_ID;
	}


	public void setCVMRM_AG_ID(Integer cVMRM_AG_ID) {
		CVMRM_AG_ID = cVMRM_AG_ID;
	}


	public CVM_MENU getCVMRM_CVM_MENU() {
		return CVMRM_CVM_MENU;
	}


	public void setCVMRM_CVM_MENU(CVM_MENU cVMRM_CVM_MENU) {
		CVMRM_CVM_MENU = cVMRM_CVM_MENU;
	}


	public CVM_ROLE getCVMRM_CVM_ROLE() {
		return CVMRM_CVM_ROLE;
	}


	public void setCVMRM_CVM_ROLE(CVM_ROLE cVMRM_CVM_ROLE) {
		CVMRM_CVM_ROLE = cVMRM_CVM_ROLE;
	}

	
	
	
	
	
}
