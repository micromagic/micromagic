package com.micromagic.app.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "CVM_VEHICLE")
public class CVM_VEHICLE implements Serializable {

	private static final long serialVersionUID = 2631027029580969002L;

	@Id
	@Column(name = "CVMV_ID", unique = true)
	private String CVMV_ID;

	@Column(name = "CVMV_DISP_NO")
	private String CVMV_DISP_NO;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CVMV_CVM_VEHICLE_TYPE")
	private CVM_VEHICLE_TYPE CVMV_CVM_VEHICLE_TYPE;

	public String getCVMV_DISP_NO() {
		return CVMV_DISP_NO;
	}

	public void setCVMV_DISP_NO(String cVMV_DISP_NO) {
		CVMV_DISP_NO = cVMV_DISP_NO;
	}

	public String getCVMV_ID() {
		return CVMV_ID;
	}

	public void setCVMV_ID(String cVMV_ID) {
		CVMV_ID = cVMV_ID;
	}

	public CVM_VEHICLE_TYPE getCVMV_CVM_VEHICLE_TYPE() {
		return CVMV_CVM_VEHICLE_TYPE;
	}

	public void setCVMV_CVM_VEHICLE_TYPE(CVM_VEHICLE_TYPE cVMV_CVM_VEHICLE_TYPE) {
		CVMV_CVM_VEHICLE_TYPE = cVMV_CVM_VEHICLE_TYPE;
	}

}
