package com.micromagic.app.service;

import com.micromagic.app.dto.ApiKeyDTO;
import com.micromagic.app.dto.UserDTO;

public interface AuthService {

	ApiKeyDTO login(UserDTO userDto);

	ApiKeyDTO getUserDetails(String parameter);

	

}
