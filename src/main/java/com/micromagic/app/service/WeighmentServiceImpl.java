package com.micromagic.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.micromagic.app.dao.WeighmentDao;
import com.micromagic.app.dto.WeighMentDTO;


@Service("WeighmentService")
@Transactional(propagation = Propagation.SUPPORTS)
public class WeighmentServiceImpl implements WeighmentService{

	@Autowired
	private WeighmentDao weighmentDao;
	
	@Override
	public Integer addFirstWeight(WeighMentDTO weighMentDTO) {

		return weighmentDao.addFirstWeight(weighMentDTO);
	}

	@Override
	public WeighMentDTO getWeighmentById(Integer weightmentId) {

		return weighmentDao.getWeighmentById(weightmentId);

	
	}

}
