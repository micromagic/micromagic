package com.micromagic.app.service;

import java.util.List;

import org.springframework.util.MultiValueMap;

import com.micromagic.app.dto.LangDTO;
import com.micromagic.app.dto.MaterialDTO;
import com.micromagic.app.dto.PartyDTO;
import com.micromagic.app.dto.RoleDTO;
import com.micromagic.app.dto.VehicleDTO;


public interface LovService {

	List<LangDTO> getLangList();

	List<RoleDTO> getRoleList(String userId);

	List<VehicleDTO> getVehicleListByKey(String key);

	List<PartyDTO> getPartyListByKey(String key);

	List<MaterialDTO> getMaterialListByKey(String key);

	Integer getNextTicketNo();

	List<VehicleDTO> getVehicleList();

}
