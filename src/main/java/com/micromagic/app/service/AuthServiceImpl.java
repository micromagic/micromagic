package com.micromagic.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.micromagic.app.dao.AuthDao;
import com.micromagic.app.dto.ApiKeyDTO;
import com.micromagic.app.dto.UserDTO;

@Service("AuthService")
@Transactional(propagation = Propagation.SUPPORTS)
public class AuthServiceImpl implements AuthService{
	
	@Autowired
	private AuthDao authDao;

	@Override
	public ApiKeyDTO login(UserDTO userDto) {
		// TODO Auto-generated method stub
		return authDao.login(userDto);
	}

	@Override
	public ApiKeyDTO getUserDetails(String authToken) {

		return authDao.getUserDetails(authToken) ;
	}


	

}
