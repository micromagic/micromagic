package com.micromagic.app.service;



import com.micromagic.app.dto.WeighMentDTO;

public interface WeighmentService {

	Integer addFirstWeight(WeighMentDTO weighMentDTO);

	WeighMentDTO getWeighmentById(Integer weightmentId);

}
