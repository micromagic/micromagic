package com.micromagic.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.micromagic.app.dao.LovDao;
import com.micromagic.app.dto.LangDTO;
import com.micromagic.app.dto.MaterialDTO;
import com.micromagic.app.dto.PartyDTO;
import com.micromagic.app.dto.RoleDTO;
import com.micromagic.app.dto.VehicleDTO;

@Service("LovService")
@Transactional(propagation = Propagation.SUPPORTS)
public class LovServiceImpl implements LovService {
	
	
	@Autowired
	private LovDao lovDao;
	

	@Override
	public List<LangDTO> getLangList() {
		return lovDao.getLangList();
	}


	@Override
	public List<RoleDTO> getRoleList(String userId) {
		return lovDao.getRoleList(userId);
	}


	@Override
	public List<VehicleDTO> getVehicleListByKey(String key) {
		return lovDao.getVehicleListByKey(key);
	}


	@Override
	public List<PartyDTO> getPartyListByKey(String key) {

		return lovDao.getPartyListByKey(key);
	}


	@Override
	public List<MaterialDTO> getMaterialListByKey(String key) {
		// TODO Auto-generated method stub
		return lovDao. getMaterialListByKey(key);
	}


	@Override
	public Integer getNextTicketNo() {

		return lovDao.getNextTicketNo();
	}


	@Override
	public List<VehicleDTO> getVehicleList() {

		return lovDao.getVehicleList();
	}

}
