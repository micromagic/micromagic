package com.micromagic.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;


@SpringBootApplication
@EnableSwagger2
public class MicroMagicApplication  extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(MicroMagicApplication.class, args);
	}
	
	@Bean
	   public Docket productApi() {
	      return new Docket(DocumentationType.SWAGGER_12)
	    		  .select()
	    		  .apis(RequestHandlerSelectors.any())
	    		  .paths(regex("/api.*"))
	    		  .build();
	   }

}
