package com.micromagic.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.micromagic.app.dto.WeighMentDTO;
import com.micromagic.app.service.WeighmentService;

@CrossOrigin
@Controller
@RequestMapping("api/weightment")
public class WeighmentController {

	@Autowired
	private WeighmentService weighmentService;
	
	@PostMapping("/first/weight")
	private ResponseEntity<?> firstWeight(@RequestBody WeighMentDTO weighMentDTO,@RequestHeader("X-Auth-Token") String xAuthToken) {
		return new ResponseEntity<Object>(weighmentService.addFirstWeight(weighMentDTO), HttpStatus.OK);
	}

	
	@GetMapping("/detail")
	private ResponseEntity<?> requestWeightDetails(@RequestParam Integer weightmentId,@RequestHeader("X-Auth-Token") String xAuthToken) {
		return new ResponseEntity<Object>(weighmentService.getWeighmentById(weightmentId), HttpStatus.OK);
	}
	
	
}
