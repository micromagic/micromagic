package com.micromagic.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import com.micromagic.app.service.LovService;

@CrossOrigin
@Controller
@RequestMapping("api/lov/")
public class LovController {

	@Autowired
	private LovService lovService;

	@GetMapping("/lang/list")
	private ResponseEntity<?> login() {
		return new ResponseEntity<Object>(lovService.getLangList(), HttpStatus.OK);
	}

	
	
	@GetMapping("/next/ticket/no")
	private ResponseEntity<?> getNextTicketNo(@RequestHeader("X-Auth-Token") String xAuthToken) {
		return new ResponseEntity<Object>(lovService.getNextTicketNo(), HttpStatus.OK);
	}
	
	
	@GetMapping("/role/list")
	private ResponseEntity<?> roleList(@RequestAttribute("userId") String userId,
			@RequestAttribute("roleId") String roleId, @RequestHeader("X-Auth-Token") String xAuthToken) {	
		return new ResponseEntity<Object>(lovService.getRoleList(userId), HttpStatus.OK);
	}
}
