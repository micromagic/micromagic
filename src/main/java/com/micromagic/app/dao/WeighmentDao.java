package com.micromagic.app.dao;

import com.micromagic.app.dto.WeighMentDTO;

public interface WeighmentDao {

	Integer addFirstWeight(WeighMentDTO weighMentDTO);

	WeighMentDTO getWeighmentById(Integer weightmentId);

}
