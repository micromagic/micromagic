package com.micromagic.app.dao;

import java.time.OffsetDateTime;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.micromagic.app.dto.VehicleDTO;
import com.micromagic.app.dto.WeighMentDTO;
import com.micromagic.app.entity.CVM_MATERIAL;
import com.micromagic.app.entity.CVM_PARTY;
import com.micromagic.app.entity.CVM_VEHICLE;
import com.micromagic.app.entity.CVT_WEIGHMENT;

@Repository("WeighmentDao")
@Transactional
public class WeighmentDaoImpl implements WeighmentDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Integer addFirstWeight(WeighMentDTO weighMentDTO) {

		String vehicleid = weighMentDTO.getVehicleNumber().getVehicleNo().replaceAll("\\s+", "").trim();

		// If new vehicle then Add
		if (sessionFactory.getCurrentSession().get(CVM_VEHICLE.class, vehicleid) == null) {
			CVM_VEHICLE vehicle = new CVM_VEHICLE();
			vehicle.setCVMV_ID(weighMentDTO.getVehicleNumber().getVehicleNo().replaceAll("\\s+", "").trim());
			vehicle.setCVMV_CVM_VEHICLE_TYPE(null);
			vehicle.setCVMV_DISP_NO(weighMentDTO.getVehicleNumber().getVehicleNo());
			sessionFactory.getCurrentSession().save(vehicle);
		}

		// if new party then add
		if (sessionFactory.getCurrentSession().get(CVM_PARTY.class, weighMentDTO.getPartyName().getPartyId()) == null) {

			CVM_PARTY party = new CVM_PARTY();
			party.setCVMP_ID(weighMentDTO.getPartyName().getPartyId().trim());
			sessionFactory.getCurrentSession().save(party);

		}

		// If New Material then Add
		if (sessionFactory.getCurrentSession().get(CVM_MATERIAL.class,
				weighMentDTO.getMaterialName().getMaterialId()) == null) {

			CVM_MATERIAL material = new CVM_MATERIAL();
			material.setCVMM_ID(weighMentDTO.getMaterialName().getMaterialId());
			sessionFactory.getCurrentSession().save(material);

		}

		CVT_WEIGHMENT weighment = new CVT_WEIGHMENT();
		weighment.setCVTW_VEHICLE_NUMBER(vehicleid);
		weighment.setCVTW_PARTY(weighMentDTO.getPartyName().getPartyId());
		weighment.setCVTW_ITEM_NAME(weighMentDTO.getMaterialName().getMaterialId());
		weighment.setCVTW_FEES(weighMentDTO.getAmount());
		weighment.setCVTW_TARE(weighMentDTO.getTareWt());
		weighment.setCVTW_NET(weighMentDTO.getNetWt());
		weighment.setCVTW_TARE_DT(OffsetDateTime.now());
		weighment.setCVTW_GROSS_DT(OffsetDateTime.now());

		return (Integer) sessionFactory.getCurrentSession().save(weighment);
	}

	@Override
	public WeighMentDTO getWeighmentById(Integer weightmentId) {

		CVT_WEIGHMENT weighment = sessionFactory.getCurrentSession().get(CVT_WEIGHMENT.class, weightmentId);

		if (weighment == null) {
			
			return null;
			
		}
		
		WeighMentDTO weighMentDTO = new WeighMentDTO();
		
		VehicleDTO vehicleDTO = new VehicleDTO();
		
		vehicleDTO.setVehicleNo(weighment.getCVTW_VEHICLE_NUMBER());
		
		weighMentDTO.setVehicleNumber(vehicleDTO);
		
		
		return weighMentDTO;
	}

}
