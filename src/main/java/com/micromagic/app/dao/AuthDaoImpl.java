package com.micromagic.app.dao;

import java.time.OffsetDateTime;
import java.util.Calendar;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.micromagic.app.dto.ApiKeyDTO;
import com.micromagic.app.dto.UserDTO;
import com.micromagic.app.entity.CVM_USER;
import com.micromagic.app.entity.CVM_USER_ROLE;
import com.micromagic.app.entity.CVT_API_KEY;

@Repository("AuthDao")
@Transactional
public class AuthDaoImpl implements AuthDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ApiKeyDTO login(UserDTO userDto) {

		CVM_USER user = sessionFactory.getCurrentSession().get(CVM_USER.class, userDto.getUserId());

		if (user == null) {
			return null;
		}

		if (user.getCVMU_DELETED_YN().equals("Y")) {

			return null;
		}

		if (!new BCryptPasswordEncoder().matches(userDto.getPassword(), user.getCVMU_PASSWORD())) {

			return null;
		}

		CVT_API_KEY api_key = new CVT_API_KEY();

		api_key.setCVTAK_CVM_USER(user);

		Query<?> query = sessionFactory.getCurrentSession()
				.createQuery(
						"FROM CVM_USER_ROLE WHERE CVMUR_CVM_USER.CVMU_ID =:userId AND CVMUR_DEFAULT_YN =:defalutYN")
				.setParameter("userId", user.getCVMU_ID()).setParameter("defalutYN", "Y").setMaxResults(1);

		CVM_USER_ROLE userRole = (CVM_USER_ROLE) query.uniqueResult();

		if (userRole == null) {
			return null;
		}

		api_key.setCVTAK_CVM_ROLE(userRole.getCVMUR_CVM_ROLE());
		api_key.setCVTAK_KEY(
				new BCryptPasswordEncoder().encode(String.valueOf(Calendar.getInstance().getTimeInMillis())));
		api_key.setCVTAK_CR_DT(OffsetDateTime.now());
		sessionFactory.getCurrentSession().save(api_key);

		return new ApiKeyDTO(api_key);
	}

	@Override
	public ApiKeyDTO getUserDetails(String authToken) {

		CVT_API_KEY apiKey = sessionFactory.getCurrentSession().get(CVT_API_KEY.class, authToken);

		if (apiKey == null) {
			return null;
		}

		ApiKeyDTO apiKeyDTO = new ApiKeyDTO();
		
		
		apiKeyDTO.setUserId(apiKey.getCVTAK_CVM_USER().getCVMU_ID());
		
		apiKeyDTO.setRoleId(apiKey.getCVTAK_CVM_ROLE().getCVMR_ID());
		
		return apiKeyDTO;
	}

}
