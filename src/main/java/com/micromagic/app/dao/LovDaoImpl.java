package com.micromagic.app.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.micromagic.app.dto.LangDTO;
import com.micromagic.app.dto.MaterialDTO;
import com.micromagic.app.dto.PartyDTO;
import com.micromagic.app.dto.RoleDTO;
import com.micromagic.app.dto.VehicleDTO;
import com.micromagic.app.entity.CVM_LANG;
import com.micromagic.app.entity.CVM_MATERIAL;
import com.micromagic.app.entity.CVM_PARTY;
import com.micromagic.app.entity.CVM_ROLE;
import com.micromagic.app.entity.CVM_USER_ROLE;
import com.micromagic.app.entity.CVM_VEHICLE;

@Repository("LovDao")
@Transactional
public class LovDaoImpl implements LovDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<LangDTO> getLangList() {

		List<LangDTO> langDtoList = new ArrayList<>();

		Query<?> query = sessionFactory.getCurrentSession().createQuery("FROM CVM_LANG");

		List<CVM_LANG> langList = (List<CVM_LANG>) query.list();

		for (CVM_LANG lang : langList) {

			langDtoList.add(new LangDTO(lang));

		}

		return langDtoList;
	}

	@Override
	public List<RoleDTO> getRoleList(String userId) {

		@SuppressWarnings("unchecked")
		List<CVM_ROLE> userRoleList = (List<CVM_ROLE>) sessionFactory.getCurrentSession()
				.createQuery("SELECT CVMUR_CVM_ROLE FROM CVM_USER_ROLE WHERE CVMUR_CVM_USER =:userId")
				.setParameter("userId", userId).list();

		List<RoleDTO> roleDtoList = new ArrayList<RoleDTO>();

		for (CVM_ROLE role : userRoleList) {

			RoleDTO roleDTO = new RoleDTO();

			roleDTO.setRoleId(role.getCVMR_ID());
			roleDTO.setRolename(role.getCVMR_NAME());

			roleDtoList.add(roleDTO);
		}

		return roleDtoList;
	}

	@Override
	public List<VehicleDTO> getVehicleListByKey(String key) {

		Query<?> query = sessionFactory.getCurrentSession()
				.createQuery("FROM CVM_VEHICLE WHERE CVMV_ID like concat('%',:key,'%')").setParameter("key", key);

		List<CVM_VEHICLE> vehicleList = (List<CVM_VEHICLE>) query.list();

		List<VehicleDTO> vehicleDtoList = new ArrayList<VehicleDTO>();

		for (CVM_VEHICLE vehicle : vehicleList) {

			VehicleDTO vehicleDTO = new VehicleDTO();

			vehicleDTO.setVehicleNo(vehicle.getCVMV_ID());
			vehicleDTO.setVehicleDisp(vehicle.getCVMV_DISP_NO());

			if (vehicle.getCVMV_CVM_VEHICLE_TYPE() != null) {

				vehicleDTO.setVehicleType(vehicle.getCVMV_CVM_VEHICLE_TYPE().getCVMVT_DISC());
			}

			vehicleDtoList.add(vehicleDTO);
		}

		return vehicleDtoList;
	}

	@Override
	public List<PartyDTO> getPartyListByKey(String key) {

		Query<?> query = sessionFactory.getCurrentSession()
				.createQuery("FROM CVM_PARTY WHERE CVMP_NAME like concat('%',:key,'%')").setParameter("key", key);

		List<CVM_PARTY> partyList = (List<CVM_PARTY>) query.list();

		List<PartyDTO> partyDtoList = new ArrayList<PartyDTO>();
		
		for (CVM_PARTY party : partyList) {
			
			PartyDTO partyDTO = new PartyDTO();
			
			partyDTO.setPartyId(party.getCVMP_ID());
			partyDTO.setPartyName(party.getCVMP_NAME());
			
			partyDtoList.add(partyDTO);
			
		}

		return partyDtoList;
	}

	@Override
	public List<MaterialDTO> getMaterialListByKey(String key) {

		Query<?> query = sessionFactory.getCurrentSession()
				.createQuery("FROM CVM_MATERIAL WHERE CVMM_DISP_NO like concat('%',:key,'%')").setParameter("key", key);

		List<CVM_MATERIAL> materialList = (List<CVM_MATERIAL>) query.list();

		List<MaterialDTO> materialListDtoList = new ArrayList<MaterialDTO>();
		
		for (CVM_MATERIAL material : materialList) {
			
			MaterialDTO materialDTO = new MaterialDTO();
			
			materialDTO.setMaterialId(material.getCVMM_ID());
			materialDTO.setMaterialName(material.getCVMM_NAME());
			
			materialListDtoList.add(materialDTO);
			
		}
		
		
		return materialListDtoList;
	}

	@Override
	public Integer getNextTicketNo() {

		return 1;
	}

	@Override
	public List<VehicleDTO> getVehicleList() {

	
		Query<?> query = sessionFactory.getCurrentSession()
				.createQuery("FROM CVM_VEHICLE");

		
		List<CVM_VEHICLE> vehicleList = new ArrayList<CVM_VEHICLE>();
		
		List<VehicleDTO> vehicleDtoList =  new ArrayList<VehicleDTO>();
		
		for (CVM_VEHICLE vehicle : vehicleList) {
			
			vehicleDtoList.add(new VehicleDTO(vehicle));
		}
		
		return vehicleDtoList;
	}

}
