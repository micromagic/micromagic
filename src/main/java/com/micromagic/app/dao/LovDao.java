package com.micromagic.app.dao;

import java.util.List;

import com.micromagic.app.dto.LangDTO;
import com.micromagic.app.dto.MaterialDTO;
import com.micromagic.app.dto.PartyDTO;
import com.micromagic.app.dto.RoleDTO;
import com.micromagic.app.dto.VehicleDTO;

public interface LovDao {

	List<LangDTO> getLangList();

	List<RoleDTO> getRoleList(String userId);

	List<VehicleDTO> getVehicleListByKey(String key);

	List<PartyDTO> getPartyListByKey(String key);

	List<MaterialDTO> getMaterialListByKey(String key);

	Integer getNextTicketNo();

	List<VehicleDTO> getVehicleList();

}
