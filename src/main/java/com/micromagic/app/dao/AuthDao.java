package com.micromagic.app.dao;

import com.micromagic.app.dto.ApiKeyDTO;
import com.micromagic.app.dto.UserDTO;

public interface AuthDao {

	ApiKeyDTO login(UserDTO userDto);

	ApiKeyDTO getUserDetails(String authToken);



}
