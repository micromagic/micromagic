package com.micromagic.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.micromagic.app.dto.ApiKeyDTO;
import com.micromagic.app.service.AuthService;

@Component("MicroMagicInterceptor")
public class MicroMagicInterceptor implements HandlerInterceptor {

	@Autowired
	private AuthService userService;

	@Override
	public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Object handler) throws Exception {

		ApiKeyDTO authDto = userService.getUserDetails(httpServletRequest.getHeader("X-Auth-Token"));

		if (authDto == null) {

			return false;

		}

//		System.err.println(authDto.getUserId());
//		System.err.println(authDto.getRoleId());
//		
		httpServletRequest.setAttribute("userId", authDto.getUserId());

		httpServletRequest.setAttribute("roleId", authDto.getRoleId());

		return true;

	}

}
