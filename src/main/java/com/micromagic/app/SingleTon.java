package com.micromagic.app;

import java.util.HashMap;
import java.util.Random;

public class SingleTon {

	
	public static String getRandomnumber() {
		// It will generate 6 digit random Number.
		// from 0 to 999999
		Random rnd = new Random();
		int number = rnd.nextInt(999999);
		// this will convert any number sequence into 6 character.
		return String.format("%06d", number);

}
	
	public static HashMap<String, Object>  returnSuccess(HashMap<String, Object> map, String object) {

		map.put("message", object);
		
		return map;
	}
}
