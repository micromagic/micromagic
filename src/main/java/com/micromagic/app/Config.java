package com.micromagic.app;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.micromagic.app.entity.CVM_LANG;
import com.micromagic.app.entity.CVM_MATERIAL;
import com.micromagic.app.entity.CVM_MENU;
import com.micromagic.app.entity.CVM_PARTY;
import com.micromagic.app.entity.CVM_ROLE;
import com.micromagic.app.entity.CVM_ROLE_MENU;
import com.micromagic.app.entity.CVM_USER;
import com.micromagic.app.entity.CVM_USER_ROLE;
import com.micromagic.app.entity.CVM_VEHICLE;
import com.micromagic.app.entity.CVM_VEHICLE_TYPE;
import com.micromagic.app.entity.CVM_WEIGHBRIDGE_ROLE;
import com.micromagic.app.entity.CVM_WEIGH_BRIDGE;
import com.micromagic.app.entity.CVT_API_KEY;
import com.micromagic.app.entity.CVT_WEIGHMENT;

@Configuration
@PropertySource("classpath:db.properties")
@PropertySource("classpath:mail.properties")
@EnableTransactionManagement
@EnableAutoConfiguration(exclude = HibernateJpaAutoConfiguration.class)
@ComponentScan(basePackages = { "com.micromagic.*" })
public class Config implements WebMvcConfigurer {

	@Autowired
	private Environment env;
	
	@Autowired
	private MicroMagicInterceptor interceptor;


	@Bean
	public DataSource getMicroMagicDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		
		dataSource.setDriverClassName(env.getProperty("db.driver"));
		dataSource.setUrl(env.getProperty("db.url"));
		dataSource.setUsername(env.getProperty("db.username"));
		dataSource.setPassword(env.getProperty("db.password"));
		return dataSource;
	}

	@Bean
	public LocalSessionFactoryBean getMicroMagicSessionFactory() {
		LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
		factoryBean.setDataSource(getMicroMagicDataSource());

		Properties props = new Properties();
		props.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
		props.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
		props.put("hibernate.dialect", env.getProperty("hibernate.dialect"));

		factoryBean.setHibernateProperties(props);
		factoryBean.setAnnotatedClasses(CVM_USER.class, CVM_ROLE.class, CVM_WEIGH_BRIDGE.class, CVT_WEIGHMENT.class,
				CVM_PARTY.class, CVM_WEIGHBRIDGE_ROLE.class, CVM_LANG.class, CVM_MENU.class, CVM_ROLE_MENU.class,
				CVT_API_KEY.class,CVM_USER_ROLE.class,CVM_VEHICLE_TYPE.class,CVM_VEHICLE.class,CVM_MATERIAL.class);

		return factoryBean;
	}

	@Bean
	public HibernateTransactionManager getMMTransactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(getMicroMagicSessionFactory().getObject());
		return transactionManager;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		registry.addInterceptor(interceptor).addPathPatterns("/api/**").excludePathPatterns("");
	}
}
