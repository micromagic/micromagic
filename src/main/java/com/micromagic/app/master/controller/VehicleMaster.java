package com.micromagic.app.master.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.micromagic.app.service.LovService;

@CrossOrigin
@Controller
@RequestMapping("api/vehicle")
public class VehicleMaster {
	
	
	@Autowired
private LovService lovService;
	
	@GetMapping("/search")
	private ResponseEntity<?> login(@RequestParam (value = "key",required = true) String key,@RequestHeader("X-Auth-Token") String xAuthToken) {


			return new ResponseEntity<Object>(lovService.getVehicleListByKey(key), HttpStatus.OK);

		}

	
	@GetMapping("/list")
	private ResponseEntity<?> getVehiclelist() {
		return new ResponseEntity<Object>(lovService.getVehicleList(), HttpStatus.OK);
	}
	
	}


