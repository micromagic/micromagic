package com.micromagic.app.master.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.micromagic.app.service.LovService;

@Controller
@RequestMapping("api/material")
public class MaterialController {

	

	@Autowired
	private LovService lovService;

	@CrossOrigin
	@GetMapping("/search")
	private ResponseEntity<?> login(@RequestParam(value = "key", required = true) String key,
			@RequestHeader("X-Auth-Token") String xAuthToken) {

		return new ResponseEntity<Object>(lovService.getMaterialListByKey(key), HttpStatus.OK);

	}
}
